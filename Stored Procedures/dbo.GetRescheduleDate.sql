SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[GetRescheduleDate]
	@TaskID int = NULL,
	@TaskSetID int = NULL,
	@RescheduleDate date = NULL OUTPUT
AS

IF @TaskID IS NULL
	SET @RescheduleDate = TRY_PARSE(dbo.GetTaskSetDataValue(@TaskSetID, 6) AS date)
ELSE
	SET @RescheduleDate = TRY_PARSE(dbo.GetTaskDataValue(@TaskID, 6) AS DATE)
GO
